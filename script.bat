rem optional
rem cd "D:\"

set curr_dir_name=%~p0
set file=%1

set dir_to_work=%2
set process_to_kill=%3
set dir_to_copy_file=%4
set IP=%5
set allowed_size=%6

set file_log_mail=mail.txt

set archive_name=zipped_files.zip

call :check_file %file%

call :task_header 5
call :resync_time

call :task_header 6
call :output_all_process

call :task_header 7
call :kill_process %process_to_kill%

call :task_header "8-9"
call :delete_all_file_ext %dir_to_work%

call :task_header 10
call :archive_all_files %archive_name% , %dir_to_work%

call :task_header 11
call :copy_file "%dir_to_work%\%archive_name%" , %dir_to_copy_file%

call :task_header 12
call :check_last_day %dir_to_copy_file%

call :task_header 14
call :delete_older_archives %dir_to_copy_file%

call :task_header 15
call :check_network_connection

call :task_header 16
call :check_IP_LAN %IP%

call :shutdown_LAN_computer IP

call :task_header 17
call :display_LAN_computers

call :task_header 18
call :check_ipon_LAN

call :task_header 19
call :check_size %allowed_size%

call :task_header 20
call :check_freespace

call :task_header 21
call :write_result_systeminfo

exit /B 0

:task_header
  call :break_line
  echo "Task%~1" >> %file%
exit /B 0

:break_line
  echo. >> %file%
exit /B 0

:check_file
  setlocal
    set file_check=%~1

    if exist %file_check% (
      call :write_to_file "File exists" , %file_check%
    ) else (
      call :write_to_file "Current date: %date% Time: %time:~0,8%" , %file_check%
      call :write_to_file "File %file_check% is open" , %file_check%
    )
  endlocal

exit /B 0

:resync_time
  rem start w32tm
  w32tm /resync
  call :write_to_file "Resynced time: %time:~0,8%" , %file%

exit /B 0

:write_to_file
  echo %~1 >> %~2

exit /B 0

:output_all_process
  tasklist >> %file%

exit /B 0

:kill_process
  setlocal
    set task_to_kill=%~1
    echo %task_to_kill%

    tasklist | find "%task_to_kill%"

    if %ERRORLEVEL% equ 0 (
      taskkill /im %task_to_kill%
      echo "%task_to_kill% is killed" >> %file%
    ) else (
      echo "%task_to_kill% is not killed" >> %file%
    )
  endlocal
exit /B 0

:delete_all_file_ext
  setlocal
    set directory=%~1

    cd %directory%

    for /f %%X in ('dir /S /B *.TMP') do set /A "TMP_count+=1"
    for /f %%X in ('dir /S /B temp*.*') do set /A "temp_count+=1"

    cd %curr_dir_name%

    if "%temp_count%" == "" (
      set temp_count=0
    )

    if "%TMP_count%" == "" (
      set TMP_count=0
    )

    set /A count_deleted_files=%temp_count% + %TMP_count%

    call :write_to_file "Deleted %count_deleted_files% files" , %file%
    call :write_to_file "Number of *.TMP files: %TMP_count%" , %file%
    call :write_to_file "Number of temp*.* files: %temp_count%" , %file%

    del /f /s /q %directory%\*.TMP >> %file%
    del /f /s /q %directory%\temp*.* >> %file%
  endlocal

exit /B 0

:archive_all_files
  setlocal
    set archive_name=%~1 
    set directory=%~2

    cd %directory%

    "%ProgramFiles%\WinRAR\WinRAR.exe" a -afzip %archive_name% *.*

    cd %curr_dir_name%

    call :write_to_file "Archive: %archive_name% Time after zipping: %time:~0,8%" , %file%

  endlocal
exit /B 0

:copy_file
  setlocal
    set file_copy_path=%~1
    set dir_to_copy=%~2
    
    xcopy %file_copy_path% %dir_to_copy%

    call :write_to_file "File is copied to %dir_to_copy%" , %file%
  endlocal
exit /B 0

:send_email
  setlocal

    set subject=%~1
    set file_to_send=%~2
    set smtp_server=smtp.mailspons.com
    set port=587
    set from=pranov.lyubomir@gmail.com
    set to=089adf1f96e0429a8b08@mailspons.com

    set user=089adf1f96e0429a8b08
    set pass=a33ef8a62fb94e8b900f96d78da89fb7

    "./mailsend1.20b.exe" -4 -f %from% -t %to%^
      -sub "%subject%" -smtp %smtp_server% -domain %smtp_server%^
      -starttls -port %port% -auth -user %user% -pass %pass%^
       -msg-body %file_to_send%"

  endlocal
exit /B 0

:check_last_day
  setlocal
    set directory=%~1

    call :write_to_file "Archive:" , %file%

    set /a count=0
    for /f %%i in ('"forfiles /p "%directory%" /d -0 /s /m *.zip /c "cmd /c echo @file " "') do (
      set /a count+=1
      call :write_to_file %%i, %file%
    )

    if %count% equ 0 (
      call :write_to_file "No files found" , %file_log_mail%
      call :send_email "Task 15: logging" , %file_log_mail%

      call :write_to_file "No files!" , %file%
    ) else (
      call :write_to_file "Found!" , %file%
    )

    del %file_log_mail%
  endlocal

exit /B 0

:check_network_connection
  setlocal
    set msg=Network

    ping www.google.de -n 1 -w 1000

    if errorlevel 0 ( 
      set msg="%msg% is connected"
    ) else (
      set msg="%msg% is not connected"
    )

    call :write_to_file %msg% , %file%
  endlocal
exit /B 0

:shutdown_LAN_computer
  setlocal
    set IP_lan=%~1

    for /f "tokens=1* delims=:" %%g in ('ipconfig /all') do (
      echo %%g
    )
  endlocal
exit /B 0

:display_LAN_computers
  setlocal

    call :write_to_file "List of computers in LAN:" , %file%
    for /f "delims=\\" %%G in ('net view /all ^| find "\\"') do (
      call :write_to_file %%G,  %file%
    )

  endlocal
exit /B 0

:delete_older_archives
    setlocal
    set dir_to_del=%~1

    call :write_to_file "Archive with 30 days last modified date:" , %file%

    set /a count=0
    for /f %%i in ('"forfiles /p "%dir_to_del%" /d -30 /s /m *.zip /c "cmd /c del @file ^| echo @file " "') do (
      set /a count+=1
      call :write_to_file %%i, %file%
    )

    if %count% equ 0 (
      call :write_to_file "No files" , %file%
    ) else (
      call :write_to_file "Succesfully deleted!" , %file%
    )


  endlocal
exit /B 0

:check_IP_LAN
  setlocal
    set IP_to_find=%~1
    set delay_shutdown=300

    rem another way to parse
    rem "tokens=14"

    for /f "tokens=2 delims=:" %%i in ('ipconfig ^| findstr IPv4') do ( 
      if " %IP_to_find%" == "%%i" (
        call :write_to_file "IP in LAN is found" , %file%
        
        shutdown /s -c "Goodbye" -m \\%IP_to_find% -t %delay_shutdown%
        call :write_to_file "%IP_to_find% will be shuted down in %delay_shutdown% seconds" , %file%

        goto :end
      )
    )

    call :write_to_file "IP in LAN is not found" , %file%

    :end

  endlocal
exit /B 0

:check_ipon_LAN

  setlocal ENABLEDELAYEDEXPANSION

    for /f %%i in (ipon.txt) do ( 
      set is_found=false

      for /f "tokens=2 delims=:" %%a in ('ipconfig ^| findstr IPv4') do (
        if " %%i" == "%%a" (
          set is_found=true
        )
      )

      
      if !is_found! == false (
        set msg="%%i is not found in LAN servers"
        call :write_to_file !msg! %file%
        call :write_to_file !msg! %file_log_mail%
      )
    )

  call :send_email "Task 18: logging" , %file_log_mail%
  del %file_log_mail%

  endlocal

exit /B 0

:check_size
  setlocal
    set size_allowed=%~1

    for /f "usebackq" %%A in ('%file%') do set size_file=%%~zA

    if %size_file% lss %size_allowed% (
      call :write_to_file "The size of log is less than %size_allowed%" , %file%
    ) else (
      set msg="The size of log is greater than %size_allowed%"
      call :write_to_file "%msg%" , %file%

      call :write_to_file  "%msg%" , %file_log_mail%
      call :send_email "Task 19: logging" , %file_log_mail%
      del %file_log_mail%
    )

  endlocal
exit /B 0

:check_freespace
  setlocal ENABLEDELAYEDEXPANSION

    set count=0

    for /f "tokens=1-3" %%a in ('WMIC LOGICALDISK GET FreeSpace^,Name^,Size ^|FINDSTR /I /V "Name"') do (
      set /a count=!count! + 1

      if !count! leq 2 (
        set total_space=%%c
        set free_space=%%a
        set name=%%b

        set /a f_total_space=!total_space:~0,-4! / 1074
        set /a f_free_space=!free_space:~0,-4! / 1074
        set /a consumed_space=!f_total_space! - !f_free_space!

        set f_total_space=!f_total_space:~0,-2!,!f_total_space:~-2! GB
        set f_free_space=!f_free_space:~0,-2!,!f_free_space:~-2! GB
        set f_consumed_space=!consumed_space:~0,-2!,!consumed_space:~-2! GB

        call :write_to_file "Name: !name!" , %file%
        call :write_to_file "Free Space: !f_free_space!" , %file%
        call :write_to_file "Consumed Space: !f_consumed_space!" , %file%
        call :write_to_file "Total space: !f_total_space!" , %file%

        call :write_to_file "Name: !name!" , %file_log_mail%
        call :write_to_file "Free Space: !f_free_space!" , %file_log_mail%
        call :write_to_file "Consumed Space: !f_consumed_space!" , %file_log_mail%
        call :write_to_file "Total space: !f_total_space!" , %file_log_mail%
      )

    )

    REM call :send_email "Task 20: logging" , %file_log_mail%
    del %file_log_mail%
  endlocal
exit /B 0

:write_result_systeminfo
  setlocal
    set cur_h=%time:~0,2%
    set cur_m=%time:~3,2%
    set cur_s=%time:~6,2%

    set day=%date:~4,2%
    set month=%date:~7,2%
    set year=%date:~-4%

    set name_file="systeminfo+%day%-%month%-%year%-%cur_h%-%cur_m%-%cur_s%.txt"
    
    systeminfo > %name_file%

    call :write_to_file ""systeminfo" is successfully written" , %file%
  endlocal
exit /B 0

rem pause